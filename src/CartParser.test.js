import CartParser from './CartParser';
import { readFileSync } from 'fs';

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {

	describe('parse', () => {
		let parse;
		beforeEach(() => {
			parse = parser.parse.bind(parser);
		});

		it('should throw "Validation failed!" error when validate return not empty array', () => {
			parser.readFile = () => '';
			parser.validate = jest.fn(() => ['error']);

			expect(parse).toThrow('Validation failed!');
		})

		it('shouldn\'t throw any error with valid data', () => {
			parser.readFile = () => ['Product name,Price,Quantity', 'Mobile,2,2'].join('\n');

			expect(parse).not.toThrow('Validation failed!');
		})

		it('should call each of another dependent functions one time with content of one product', () => {
			parser.readFile = jest.fn(() => ['Product name,Price,Quantity', 'Coconut,2,2'].join('\n'));
			parser.validate = jest.fn(() => []);
			parser.parseLine = jest.fn();
			parser.calcTotal = jest.fn(() => 0);

			parse('test');

			expect(parser.readFile).toHaveBeenCalledTimes(1);
			expect(parser.validate).toHaveBeenCalledTimes(1);
			expect(parser.parseLine).toHaveBeenCalledTimes(1);
			expect(parser.calcTotal).toHaveBeenCalledTimes(1);
		});
	});

	describe('validate', () => {
		let validate;

		beforeEach(() => {
			validate = parser.validate.bind(parser);
		})

		it("should return empty array when content is valid", () => {
			const content = ['Product name,Price,Quantity', 'Coconut,2,2'].join('\n');

			const errors = validate(content);

			expect(errors).toEqual([]);
		});

		it("should validate errors with incorrect columns headers", () => {
			const wrongContent = "Product name,Amount,Total!!!";
			parser.createError = jest.fn();

			validate(wrongContent);

			expect(parser.createError).toHaveBeenCalledTimes(2);
		});

		it('should validate an error if there is not appropriate amount of cells in row', () => {
			const wrongContent = ['Product name,Price,Quantity', 'Mobile,2'].join('\n');
			parser.createError = jest.fn();

			validate(wrongContent);

			expect(parser.createError).toHaveBeenCalledTimes(1);
		});

		it('should validate an error if first cell in row is an empty string', () => {
			const wrongContent = ['Product name,Price,Quantity', ',2,2'].join('\n');
			parser.createError = jest.fn();

			validate(wrongContent);

			expect(parser.createError).toHaveBeenCalledTimes(1);
		});

		it('should validate an error if there is not number at second column', () => {
			const wrongContent = ['Product name,Price,Quantity', 'Mobile,true,2'].join('\n');
			parser.createError = jest.fn();

			validate(wrongContent);

			expect(parser.createError).toHaveBeenCalledTimes(1);
		});

		it('should validate an error if there is NaN in a CSV line', () => {
			const wrongContent = ['Product name,Price,Quantity', 'Mobile,1/0,2'].join('\n');
			parser.createError = jest.fn();

			validate(wrongContent);

			expect(parser.createError).toHaveBeenCalledTimes(1);
		});

		it('should validate an error if there is negative number in a CSV line', () => {
			const wrongContent = ['Product name,Price,Quantity', 'Mobile,8,-1'].join('\n');
			parser.createError = jest.fn();

			validate(wrongContent);

			expect(parser.createError).toHaveBeenCalledTimes(1);
		});
	});

	describe('parseLine', () => {
		it('should right parse CSV line to object with id property rounding off numbers without extra zeros', () => {
			const row = 'test,2.00,56';
			const matchedResult = {
				name: 'test',
				price: 2,
				quantity: 56
			};

			const result = parser.parseLine(row);

			expect(result).toMatchObject(matchedResult);
			expect(result).toHaveProperty('id');
		});
	})
});

describe('CartParser - integration test', () => {
	it('should parse the content of a CSV file and return JSON object with products items and total price', () => {
		const filePath = 'samples/cart.csv';
		const { items, total } = require('../samples/cart.json');
		const matchedResult = {
			items: items.map(({ id, ...data }) => data),
			total
		};

		const result = parser.parse(filePath);

		expect(result).toMatchObject(matchedResult);
	});
});